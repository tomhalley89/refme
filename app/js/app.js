"use strict";

angular
    .module('RefMe', [
        "ngRoute",
        "RefMe.Controllers",
        "RefMe.Services",
        "RefMe.Factories"
    ])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/', {templateUrl: 'partials/home.html', controller: 'HomeController'});
        $routeProvider.otherwise({redirectTo: '/'});
    }]);

angular.module("RefMe.Controllers", []);
angular.module("RefMe.Factories", []);
angular.module("RefMe.Services", []);