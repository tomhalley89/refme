/**
 * Service for manipulating a shopping list
 */
angular.module("RefMe.Services")
    .service("ShoppingListService", function() {
        "use strict";

        var shoppingList = [];

        /**
         * Get shopping list
         *
         * @returns {Array}
         */
        this.getShoppingList = function() {
            return shoppingList;
        };

        /**
         * Add an item to the shopping list
         *
         * @param item
         */
        this.addToShoppingList = function(item) {
            if(typeof(item) !== 'object') {
                throw new Error("Item added to list was not of type object");
            }

            if(item.text.trim() !== '') {
                shoppingList.unshift(item);
            }
        };

        /**
         * Update an item in the shopping list
         *
         * @param original
         * @param updated
         */
        this.updateItemInShoppingList = function(original, updated) {
            var index = shoppingList.indexOf(original);

            if(index !== -1) {
                shoppingList[index] = updated;
            }
        };

        /**
         * Remove an item from the shopping list
         *
         * @param item
         */
        this.removeFromShoppingList = function(item) {
            var index = shoppingList.indexOf(item);

            if(index !== -1) {
                shoppingList.splice(index, 1);
            }
        };
    });