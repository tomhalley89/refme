/**
 * Factory for ShoppingListItem for use with ShoppingListService
 */
angular.module("RefMe.Factories")
    .factory("ShoppingListItem", function() {
        "use strict";

        return function(text) {
            this.text = (text === undefined || text === null) ? '' : text;
            this.editMode = false;
        };
    });