/**
 * Home page controller
 */
angular.module("RefMe.Controllers")
    .controller("HomeController", function($scope, ShoppingListService, ShoppingListItem) {
        "use strict";

        $scope.data = {
            shoppingList: ShoppingListService.getShoppingList(),
            newListItem: '',
            itemInEdit: ''
        };

        $scope.methods = {
            addItemToList: function() {
                // Create new ShoppingListItem
                var item  = new ShoppingListItem($scope.data.newListItem);

                // Add item to list
                ShoppingListService.addToShoppingList(item);

                // Update List from Service
                $scope.data.newListItem = '';
                $scope.data.shoppingList = ShoppingListService.getShoppingList();
            },
            updateItem: function(item) {
                // Pass in string from edited field
                var updatedItem = new ShoppingListItem($scope.data.itemInEdit.text);

                // Save changes to service
                ShoppingListService.updateItemInShoppingList(item, updatedItem);

                // Update List from Service
                $scope.data.shoppingList = ShoppingListService.getShoppingList();
            },
            deleteItemFromList: function(item) {
                // Remove item from list
                ShoppingListService.removeFromShoppingList(item);

                // Update list from service
                $scope.data.shoppingList = ShoppingListService.getShoppingList();
            },
            toggleEdit: function (item) {
                // Set all objects except target to editMode = false
                for(var i = 0; i < $scope.data.shoppingList.length; i++) {
                    if($scope.data.shoppingList[i] !== item) {
                        $scope.data.shoppingList[i].editMode = false;
                    }
                }

                // Toggle editMode on Target
                item.editMode = !item.editMode;

                // Set itemInEdit to current item
                $scope.data.itemInEdit = item;
            }
        };
    });