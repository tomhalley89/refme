"use strict";

describe("RefMe", function() {
    beforeEach(module('RefMe'));

    describe("Services", function() {
        describe("ShoppingListService", function() {
            var ShoppingListService;
            var ShoppingListItem;

            beforeEach(inject(function (_ShoppingListService_, _ShoppingListItem_) {
                ShoppingListService = _ShoppingListService_;
                ShoppingListItem = _ShoppingListItem_;
            }));

            it("should exist", function() {
                expect(ShoppingListService).toBeDefined();
            });

            describe("getShoppingList()", function() {
                it("should be defined", function() {
                    expect(ShoppingListService.getShoppingList).toBeDefined();
                });

                it("should retrieve the shopping list", function() {
                    var item = new ShoppingListItem("Cheese");

                    ShoppingListService.addToShoppingList(item);

                    var list = ShoppingListService.getShoppingList();

                    expect(list).toEqual([item]);
                });

                it("should return an empty array if no list", function() {
                    expect(ShoppingListService.getShoppingList()).toEqual([]);
                });
            });

            describe("addToShoppingList()", function() {
                it("should be defined", function() {
                    expect(ShoppingListService.addToShoppingList).toBeDefined();
                });

                it("should add an item to a list", function() {
                    spyOn(ShoppingListService, 'addToShoppingList');

                    var item = new ShoppingListItem("Bread");

                    ShoppingListService.addToShoppingList(item);

                    expect(ShoppingListService.addToShoppingList).toHaveBeenCalledWith(item);
                });

                it("should not allow adding an empty item to the list", function() {
                    var item = new ShoppingListItem(' ');

                    ShoppingListService.addToShoppingList(item);

                    expect(ShoppingListService.getShoppingList()).toEqual([]);
                });
            });

            describe("updateItemInShoppingList()", function() {
                it("should be defined", function() {
                    expect(ShoppingListService.updateItemInShoppingList).toBeDefined();
                });

                it("should update an item in the shopping list", function() {
                    var item1 = new ShoppingListItem("Bread");
                    var item2 = new ShoppingListItem("Cheese");
                    var item3 = new ShoppingListItem("Milk");

                    ShoppingListService.addToShoppingList(item1);
                    ShoppingListService.addToShoppingList(item2);
                    ShoppingListService.addToShoppingList(item3);

                    // Replace Bread with Eggs
                    var updatedItem = new ShoppingListItem("Eggs");

                    ShoppingListService.updateItemInShoppingList(item1, updatedItem);

                    var list = ShoppingListService.getShoppingList();

                    expect(list[0].text).toEqual("Milk");
                    expect(list[1].text).toEqual("Cheese");
                    expect(list[2].text).toEqual("Eggs");
                });

                it("should handle error if item not in list", function() {
                    var item1 = new ShoppingListItem("Bread");
                    var updatedItem = new ShoppingListItem("Eggs");

                    ShoppingListService.updateItemInShoppingList(item1, updatedItem);

                    expect(ShoppingListService.getShoppingList()).toEqual([]);
                });
            });

            describe("removeFromShoppingList()", function() {
                it("should be defined", function() {
                    expect(ShoppingListService.removeFromShoppingList).toBeDefined();
                });

                it("should remove an item from the shopping list", function() {
                    var item1 = new ShoppingListItem("Bread");
                    var item2 = new ShoppingListItem("Cheese");
                    var item3 = new ShoppingListItem("Milk");

                    ShoppingListService.addToShoppingList(item1);
                    ShoppingListService.addToShoppingList(item2);
                    ShoppingListService.addToShoppingList(item3);

                    ShoppingListService.removeFromShoppingList(item1);

                    expect(ShoppingListService.getShoppingList()).toEqual([item3, item2]);
                });

                it("should handle error if item not in list", function() {
                    var item1 = new ShoppingListItem("Bread");
                    var item2 = new ShoppingListItem("Cheese");
                    var item3 = new ShoppingListItem("Milk");

                    ShoppingListService.addToShoppingList(item1);
                    ShoppingListService.addToShoppingList(item2);
                    ShoppingListService.addToShoppingList(item3);

                    var ghostItem = new ShoppingListItem("Phantom");
                    ShoppingListService.removeFromShoppingList(ghostItem);

                    expect(ShoppingListService.getShoppingList()).toEqual([item3, item2, item1]);
                });
            });
        });
    });
});
