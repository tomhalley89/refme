"use strict";

describe("RefMe", function() {
    beforeEach(module('RefMe'));

    describe("Controllers", function() {
        describe("HomeController", function() {
            var HomeController,
                $scope;

            /**
             * Taken from http://jsfiddle.net/fdietz/2Ny8x/
             */
            beforeEach(inject(function ($rootScope, $controller) {
                $scope = $rootScope.$new();
                HomeController = $controller('HomeController', {
                    '$scope': $scope
                });
            }));

            it("should be defined", function() {
                expect(HomeController).toBeDefined();
            });

            describe("$scope.data", function() {
                it("should be defined", function() {
                    expect($scope.data).toBeDefined();
                });
            });

            describe("$scope.methods", function() {
                it("should be defined", function() {
                    expect($scope.methods).toBeDefined();
                });

                describe("addItemToList()", function() {
                    it("should be defined", function() {
                        expect($scope.methods.addItemToList).toBeDefined();
                    });

                    it("should add item to $scope.data.shoppingList", function() {
                        $scope.data.newListItem = "Bread";
                        $scope.methods.addItemToList();

                        expect($scope.data.shoppingList[0].text).toEqual("Bread");
                    });

                    it("should update $scope.data.newListItem with blank value", function() {
                        $scope.data.newListItem = "Bread";
                        $scope.methods.addItemToList();

                        expect($scope.data.newListItem).toEqual("");
                    });
                });

                describe("updateItem()", function() {
                    var ShoppingListService,
                        ShoppingListItem;

                    beforeEach(inject(function (_ShoppingListService_,_ShoppingListItem_) {
                        ShoppingListService = _ShoppingListService_;
                        ShoppingListItem = _ShoppingListItem_;
                    }));

                    it("should be defined", function() {
                        expect($scope.methods.updateItem).toBeDefined();
                    });

                    it("should update an item in the shopping list", function() {

                        var originalItem = new ShoppingListItem("Cheese");
                        ShoppingListService.addToShoppingList(originalItem);

                        var updatedItem = new ShoppingListItem("Milk");

                        $scope.data.itemInEdit = updatedItem;
                        $scope.methods.updateItem(originalItem);

                        expect($scope.data.shoppingList).toEqual([updatedItem]);
                    });

                    it("should handle error if item no longer in list", function() {
                        var originalItem = new ShoppingListItem("Cheese");

                        // Skip adding to Service

                        $scope.data.itemInEdit = new ShoppingListItem("Milk");
                        $scope.methods.updateItem(originalItem);

                        expect($scope.data.shoppingList).toEqual([]);
                    });
                });

                describe("deleteItemFromList()", function() {
                    var ShoppingListService,
                        ShoppingListItem,
                        item1,
                        item2,
                        item3;

                    beforeEach(inject(function (_ShoppingListService_, _ShoppingListItem_) {
                        ShoppingListService = _ShoppingListService_;
                        ShoppingListItem = _ShoppingListItem_;

                        item1 = new ShoppingListItem("Milk");
                        item2 = new ShoppingListItem("Eggs");
                        item3 = new ShoppingListItem("Cheese");

                        ShoppingListService.addToShoppingList(item1);
                        ShoppingListService.addToShoppingList(item2);
                        ShoppingListService.addToShoppingList(item3);
                    }));

                    it("should be defined", function() {
                        expect($scope.methods.deleteItemFromList).toBeDefined();
                    });

                    it("should delete an item from $scope.data.shoppingList", function() {
                        $scope.methods.deleteItemFromList(item2);

                        expect($scope.data.shoppingList).toEqual([item3, item1]);
                    });

                    it("should handle error when deleting item that doesn't exist", function() {
                        var ghostItem = new ShoppingListItem("Phantom");

                        $scope.methods.deleteItemFromList(ghostItem);

                        expect($scope.data.shoppingList).toEqual([item3, item2, item1]);
                    });
                });

                describe("toggleEdit()", function() {
                    var ShoppingListService,
                        ShoppingListItem,
                        item1,
                        item2,
                        item3;

                    beforeEach(inject(function (_ShoppingListService_, _ShoppingListItem_) {
                        ShoppingListService = _ShoppingListService_;
                        ShoppingListItem = _ShoppingListItem_;

                        $scope.data.newListItem = "Milk";
                        $scope.methods.addItemToList();

                        $scope.data.newListItem = "Eggs";
                        $scope.methods.addItemToList();

                        $scope.data.newListItem = "Cheese";
                        $scope.methods.addItemToList();

                        item1 = $scope.data.shoppingList[2];
                        item2 = $scope.data.shoppingList[1];
                        item3 = $scope.data.shoppingList[0];
                    }));

                    it("should be defined", function() {
                        expect($scope.methods.toggleEdit).toBeDefined();
                    });

                    it("should set an item to editMode = true", function() {
                        $scope.methods.toggleEdit(item1);

                        expect($scope.data.shoppingList[2].editMode).toEqual(true)
                    });

                    it("should set all objects editMode to false except target", function() {
                        $scope.methods.toggleEdit(item1);
                        expect($scope.data.shoppingList[2].editMode).toEqual(true);

                        $scope.methods.toggleEdit(item2);
                        expect($scope.data.shoppingList[2].editMode).toEqual(false);
                    });
                });
            });
        });
    });
});
