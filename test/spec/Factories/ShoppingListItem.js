"use strict";

describe("RefMe", function() {
    beforeEach(module('RefMe'));

    describe("Factories", function() {
        describe("ShoppingListItem", function() {
            var ShoppingListItem;

            beforeEach(inject(function (_ShoppingListItem_) {
                ShoppingListItem = _ShoppingListItem_;
            }));

            it("should be defined", function() {
                expect(ShoppingListItem).toBeDefined();
            });

            it("should be instantiable", function() {
                expect(new ShoppingListItem()).toBeDefined();
            });

            it("should construct with a text value", function() {
                var item = new ShoppingListItem("Bread");
                expect(item.text).toEqual("Bread");
            });

            describe("text", function() {
                it("should be defined", function() {
                    var item = new ShoppingListItem();

                    expect(item.text).toBeDefined();
                });

                it("should be store a string value", function() {
                    var item = new ShoppingListItem();
                    item.text = "Bread";

                    expect(item.text).toEqual("Bread");
                });
            });

            describe("editMode", function() {
                it("should be defined", function() {
                    var item = new ShoppingListItem();

                    expect(item.editMode).toBeDefined();
                });

                it("should be false by default", function() {
                    var item = new ShoppingListItem();

                    expect(item.editMode).toEqual(false);
                });

                it("should be editable", function() {
                    var item = new ShoppingListItem();
                    item.editMode = true;

                    expect(item.editMode).toEqual(true);
                })
            });
        });
    });
});