# RefMe Technical Tasks #

## Running the Application ##

### Pre-Requisites ###

* npm (nodejs)

### Shopping list app ###

Run the command `npm start` from inside the project folder, then visit the following url: [http://localhost:8000/app/](http://localhost:8000/app/)

### Unit tests ###

Jasmine unit test runner can be loaded from test/SpecRunner.html

### American Flag ###

American flag HTML file can be loaded from app/stars-and-strips.html